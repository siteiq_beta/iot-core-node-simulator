
const express = require('express')
const router = express.Router()
var awsIot = require('aws-iot-device-sdk');
var zlib = require('zlib');

var device = awsIot.device({
 keyPath: 'Cert/pump.private.key',
 certPath: 'Cert/pump.cert.pem',
 caPath: 'Cert/root-CA.crt',
 host: 'a28u34da0w602w.iot.us-east-2.amazonaws.com',
 clientId: 'iot-testing',
 region: 'us-east-2'
 });

router.get('/', (req, res) => {
  res.render('index')
})

router.post('/errorcodes', (req, res) => {
  console.log(req.body.message);
  zlib.gzip(req.body.message, function (error, result) {
   if (error) throw error;
   
     console.log(result);
	 
})
  device.publish(req.body.topic, req.body.message);
  res.send(req.body.topic+' Message Successfully inserted');
})

device
 .on('connect', function() {
 console.log('connected');
 device.subscribe('rpt/errorcodes');
 //device.publish('arn:aws:iot:us-east-1:332167152351:thing/pump', JSON.stringify({ test_data: 1}));
 });
 

 device
 .on('message', function(topic, payload) {
 console.log('message', topic, payload.toString());
});

module.exports = router
