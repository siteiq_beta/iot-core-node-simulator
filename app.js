var express = require('express');
var awsIot = require('aws-iot-device-sdk');
var pako = require('pako');

var app = express();

var device = awsIot.device({
 keyPath: 'Cert/pump.private.key',
 certPath: 'Cert/pump.cert.pem',
 caPath: 'Cert/root-CA.crt',
 host: 'a28u34da0w602w.iot.us-east-2.amazonaws.com',
 clientId: 'iot-testing',
 region: 'us-east-2'
 });


app.get('/', function (req, res) {
  res.send('/rpt/errorcodes');
});

app.listen(3000, function () {
  console.log('Simulator app listening on port 3000!');
});

app.get('/errorcodes', function (req, res) {
  data=`1 All Pump 00:01:00 Sat Jan 01 2000 eDataBase EC5239:MINOR:Database init forced
2 All Pump 00:01:00 Sat Jan 01 2000 eDataBase EC5240:MINOR:Database wrong write: 330
3 All Pump 00:01:00 Sat Jan 01 2000 ePumpOperation :INFO :Pump control node NVRAM SW Reset: 3
4 All Pump 00:01:01 Sat Jan 01 2000 eSystemManager :INFO :System Manager: Read RTC and Set System Time Success
5 All Pump 00:01:01 Sat Jan 01 2000 ePumpOperation :INFO :Pump control node Online`
  /*zlib.gzip(data, function (error, result) {
   if (error) throw error;
   
     console.log(result);
	 device.publish('arn:aws:iot:us-east-1:332167152351:thing/pump', JSON.stringify({ test_data: result}));
})
  */


	//let buff = new Buffer(data);  
	//let base64data = buff.toString('base64');
	//console.log(base64data);
	device.publish('rpt/errorcodes', data);
	
  res.send('errorcodes published');
});

app.get('/pump', function (req, res) {
  data=` E500 PUMP  LON NODE VERSION 
Application Software Version     P03040 (03.0.40P)
Application Software Date        07 28 2014 (MM DD YYYY)
Application Hash Code            1D2625AA6278753B4D95A901EA8D6C8AC302F788
Application Software CRC         A9CB
MIP Version                      0414 (04.14)
DoorNodeA                        02057 (02.0.57)
DoorNodeB                        02057 (02.0.57)
DoorNodeHostA                    
DoorNodeHostB                    
Pump Boot Version                10305 (V10.3.05)
2016-08-22 13:36:11
36`
  /*zlib.gzip(data, function (error, result) {
   if (error) throw error;
   
     console.log(result);
	 device.publish('arn:aws:iot:us-east-1:332167152351:thing/pump', JSON.stringify({ test_data: result}));
})
  */


	//let buff = new Buffer(data);  
	//let base64data = buff.toString('base64');
	//console.log(base64data);
	device.publish('rpt/pump', data);
	
  res.send('pump published');
});

app.get('/performance1', function (req, res) {
  data=`Flow;
Max,Grade="1",010.42;
Max,Grade="2",011.14;
Max,Grade="3",009.82;
Max,Grade="4",000.00;
Max,Grade="5",000.00;
Max,Grade="6",011.90;
;
Transactions;
TwoWireCount,, 6906;
StandAloneCount,, 49;
PresetOverruns,, 118;
PresetUnderruns,, 5258;
;
Controller;
ErrorCount,, 7;
;
2018-05-23 18:34:50
5`
  /*zlib.gzip(data, function (error, result) {
   if (error) throw error;
   
     console.log(result);
	 device.publish('arn:aws:iot:us-east-1:332167152351:thing/pump', JSON.stringify({ test_data: result}));
})
  */


	//let buff = new Buffer(data);  
	//let base64data = buff.toString('base64');
	//console.log(base64data);
	device.publish('rpt/performance1', data);
	
  res.send('performance1 published');
});

app.get('/performance2', function (req, res) {
  data=`Flow;
Max,Grade="1",010.42;
Max,Grade="2",011.14;
Max,Grade="3",009.82;
Max,Grade="4",000.00;
Max,Grade="5",000.00;
Max,Grade="6",011.90;
;
Transactions;
TwoWireCount,, 6906;
StandAloneCount,, 49;
PresetOverruns,, 118;
PresetUnderruns,, 5258;
;
Controller;
ErrorCount,, 7;
;
2018-05-23 18:34:50
3`
  /*zlib.gzip(data, function (error, result) {
   if (error) throw error;
   
     console.log(result);
	 device.publish('arn:aws:iot:us-east-1:332167152351:thing/pump', JSON.stringify({ test_data: result}));
})
  */


	//let buff = new Buffer(data);  
	//let base64data = buff.toString('base64');
	//console.log(base64data);
	device.publish('rpt/performance2', data);
	
  res.send('performance2 published');
});

app.get('/weights_and_measures', function (req, res) {
  data=`11 All Pump 20:15:27 Sat Jan 15 2000 eRegulatory :INFO :Entry to ZModem Successful
12 All Pump 20:18:26 Sat Jan 15 2000 eRegulatory :INFO :Special country option (CC69) changed: Old Value = 1, NeValue = 3
13 All Pump 20:18:26 Sat Jan 15 2000 eRegulatory :INFO :Dispenser type (CC90) changed: Old Value = 1, New Value = 17
14 All Pump 20:18:26 Sat Jan 15 2000 eRegulatory :INFO :E500E500S Feature Set (CC96) changed: Old Value = 1, New Value = 5
15 All Pump 20:18:26 Sat Jan 15 2000 eRegulatory :INFO :Blend ratio (CC72) changed for grade 1: not programmed
16 All Pump 20:18:26 Sat Jan 15 2000 eRegulatory :INFO :Blend ratio (CC72) changed for grade 1: Ratio = 100
17 All Pump 20:18:26 Sat Jan 15 2000 eRegulatory :INFO :Blend ratio (CC72) changed for grade 2: not programmed
18 All Pump 20:18:26 Sat Jan 15 2000 eRegulatory :INFO :Blend ratio (CC72) changed for grade 2: Ratio = 50
19 All Pump 20:18:26 Sat Jan 15 2000 eRegulatory :INFO :Blend ratio (CC72) changed for grade 3: not programmed
20 All Pump 20:18:26 Sat Jan 15 2000 eRegulatory :INFO :Blend ratio (CC72) changed for grade 3: Ratio = 0`
  /*zlib.gzip(data, function (error, result) {
   if (error) throw error;
   
     console.log(result);
	 device.publish('arn:aws:iot:us-east-1:332167152351:thing/pump', JSON.stringify({ test_data: result}));
})
  */


	//let buff = new Buffer(data);  
	//let base64data = buff.toString('base64');
	//console.log(base64data);
	device.publish('rpt/weights_and_measures', data);
	
  res.send('weights_and_measures published');
});

device
 .on('connect', function() {
 console.log('connected');
 device.subscribe('rpt/errorcodes');
 //device.publish('arn:aws:iot:us-east-1:332167152351:thing/pump', JSON.stringify({ test_data: 1}));
 });
 
 device
 .on('connect', function() {
 console.log('connected');
 device.subscribe('rpt/pump');
 //device.publish('arn:aws:iot:us-east-1:332167152351:thing/pump', JSON.stringify({ test_data: 1}));
 });
 
  device
 .on('connect', function() {
 console.log('connected');
 device.subscribe('rpt/performance1');
 //device.publish('arn:aws:iot:us-east-1:332167152351:thing/pump', JSON.stringify({ test_data: 1}));
 });
 
   device
 .on('connect', function() {
 console.log('connected');
 device.subscribe('rpt/performance2');
 //device.publish('arn:aws:iot:us-east-1:332167152351:thing/pump', JSON.stringify({ test_data: 1}));
 });
 
    device
 .on('connect', function() {
 console.log('connected');
 device.subscribe('rpt/weights_and_measures');
 //device.publish('arn:aws:iot:us-east-1:332167152351:thing/pump', JSON.stringify({ test_data: 1}));
 });
 
device
 .on('message', function(topic, payload) {
 console.log('message', topic, payload.toString());
});